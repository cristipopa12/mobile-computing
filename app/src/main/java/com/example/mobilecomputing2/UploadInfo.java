package com.example.mobilecomputing2;

public class UploadInfo {

    public String imageName;
    public String imageURL;
    public String userId;


    public UploadInfo(String tempImageName, String s){}

    public UploadInfo(String name, String url, String userId) {
        this.imageName = name;
        this.imageURL = url;
        this.userId = userId;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImageName() {
        return imageName;
    }
    public String getImageURL() {
        return imageURL;
    }

}
