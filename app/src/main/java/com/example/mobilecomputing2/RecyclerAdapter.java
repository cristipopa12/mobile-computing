package com.example.mobilecomputing2;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {


    private static final String Tag = "RecyclerView";
    private Context mContext;
    private ArrayList<UploadInfo> uploadsList;

    public RecyclerAdapter(Context mContext, ArrayList<UploadInfo> uploadsList) {
        this.mContext = mContext;
        this.uploadsList = uploadsList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView_display;
        TextView textView_display;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView_display = itemView.findViewById(R.id.images_view_last);
            textView_display = itemView.findViewById(R.id.images_text_view_last);
        }
    }


    @NonNull
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upload_items, parent, false);

        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        // TextView
        holder.textView_display.setText(uploadsList.get(position).getImageName());

        // ImageView : Glide Library
        Glide.with(mContext).load(uploadsList.get(position).getImageURL()).into(holder.imageView_display);
        Picasso.get().load(uploadsList.get(position).getImageURL()).into(holder.imageView_display);
    }

    @Override
    public int getItemCount() {
        return uploadsList.size();
    }
}