package com.example.mobilecomputing2;


import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class All_images_Activity extends AppCompatActivity {

    // Widgets
    private RecyclerView recyclerView;

    // Firebase
    private DatabaseReference databaseReference;

    // Variables
    private ArrayList<UploadInfo> uploadsList;
    private RecyclerAdapter recyclerAdapter;
    String currentUserId;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);

        recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        currentUserId = currentFirebaseUser.getUid();


        // Firebase
        databaseReference = FirebaseDatabase.getInstance().getReference();

        // ArrayList:
        uploadsList = new ArrayList<>();

        // Clear ArrayList
        clearAll();

        // Get data method
        getDataFromFirebase();

    }

    private void getDataFromFirebase() {

        Query query = databaseReference.child("user_photo");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                clearAll();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){

                    String imageURL = snapshot.child("imageURL").getValue().toString();
                    String imageName = snapshot.child("imageName").getValue().toString();
                    String userId = snapshot.child("userId").getValue().toString();

                    UploadInfo imageFromDB = new UploadInfo(imageName,imageURL,userId);

                    Toast.makeText(All_images_Activity.this, snapshot.child("imageURL").getValue().toString(), Toast.LENGTH_SHORT).show();

                    if(currentUserId == userId){
                        uploadsList.add(imageFromDB);
                    }

                }

                recyclerAdapter = new RecyclerAdapter(getApplicationContext(), uploadsList);
                recyclerView.setAdapter(recyclerAdapter);
                recyclerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void clearAll() {

        if(uploadsList != null){
            uploadsList.clear();

            if(recyclerAdapter != null){
                recyclerAdapter.notifyDataSetChanged();
            }

        }

        uploadsList = new ArrayList<>();

    }

    @Override
    protected void onStart(){
        super.onStart();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

}