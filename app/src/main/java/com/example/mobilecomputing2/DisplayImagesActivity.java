package com.example.mobilecomputing2;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DisplayImagesActivity extends AppCompatActivity {

    // Widgets
    private RecyclerView recyclerView;

    // Firebase
    private DatabaseReference databaseReference;

    // Variables
    private ArrayList<UploadInfo> uploadsList;
    private RecyclerAdapter recyclerAdapter;
    private Context mContext;
    ImageView imageView[];
    TextView textView[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_images);

        recyclerView = findViewById(R.id.recyclerView);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        imageView[0] = findViewById(R.id.showImageView1);
        textView[0] = findViewById(R.id.showTextView1);
        imageView[1] = findViewById(R.id.showImageView2);
        textView[1] = findViewById(R.id.showTextView2);
        imageView[2] = findViewById(R.id.showImageView3);
        textView[2] = findViewById(R.id.showTextView3);
        imageView[3] = findViewById(R.id.showImageView4);
        textView[3] = findViewById(R.id.showTextView4);


        // Firebase
        databaseReference = FirebaseDatabase.getInstance().getReference();

        // ArrayList:
        uploadsList = new ArrayList<>();

        // Clear ArrayList
        clearAll();

        // Get data method
        getDataFromFirebase();

    }

    private void getDataFromFirebase() {

        Query query = databaseReference.child("user_photo");

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                clearAll();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){

                    String imageURL = snapshot.child("imageURL").getValue().toString();
                    String imageName = snapshot.child("imageName").getValue().toString();
                    String userId = snapshot.child("userId").getValue().toString();

                    UploadInfo imageFromDB = new UploadInfo(imageName,imageURL,userId);

                    //Toast.makeText(DisplayImagesActivity.this, snapshot.child("imageURL").getValue().toString(), Toast.LENGTH_SHORT).show();

                    uploadsList.add(imageFromDB);
                }

                for(int i = 0; i<= 3; i++){
                    textView[i].setText(uploadsList.get(i).getImageName());
                    Picasso.get().load(uploadsList.get(i).getImageURL()).into(imageView[i]);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void clearAll() {

        if(uploadsList != null){
            uploadsList.clear();

            if(recyclerAdapter != null){
                recyclerAdapter.notifyDataSetChanged();
            }

        }

        uploadsList = new ArrayList<>();

    }

    @Override
    protected void onStart(){
        super.onStart();
    }

}