package com.example.mobilecomputing2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegisterActivity extends AppCompatActivity {

    EditText usernameId, password, user;
    Button btnSignUp, btnMenu;
    TextView textViewSignIn;
    FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFirebaseAuth = FirebaseAuth.getInstance();
        usernameId = findViewById(R.id.register_username);
        user = findViewById(R.id.register_user);
        password = findViewById(R.id.register_password);
        btnSignUp = findViewById(R.id.register);
        btnMenu = findViewById(R.id.register_redirect_to_menu);
        textViewSignIn = findViewById(R.id.login_redirect);


        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = usernameId.getText().toString();
                String pwd = password.getText().toString();

                //Missing email
                if(email.isEmpty()){
                    usernameId.setError("Please enter an email id");
                }

                //Missing password
                else if(pwd.isEmpty()){
                    usernameId.setError("Please enter your password");
                }

                //Both missing
                else if(pwd.isEmpty() && email.isEmpty()){
                    Toast.makeText(RegisterActivity.this, "Fields are Empty!", Toast.LENGTH_SHORT).show();
                }

                else if(!pwd.isEmpty() && !email.isEmpty()){
                    mFirebaseAuth.createUserWithEmailAndPassword(email,pwd).addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(RegisterActivity.this, "SignUp Unsuccessful. Please Try Again", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                Intent intToLogin = new Intent(RegisterActivity.this, MainActivity.class);
                                startActivity(intToLogin);
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(RegisterActivity.this, "Error Occurred!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        textViewSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }
}
